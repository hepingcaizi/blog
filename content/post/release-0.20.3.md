---
title: "Vikunja frontend v0.20.3 and API v0.20.2 was released"
date: 2023-01-24T17:20:21+01:00
draft: false
---

<div class="bg-amber-400 p-4 rounded mt-4">
There has been a <a href="{{< ref "./release-0.20.4.md">}}" class="!underline">patch release for the frontend and api</a> with additional fixes.
Updating is highly encouraged.
</div>

This is a patch release with important fixes and improvements in the frontend and api.

Among code-improvements, refactorings and dependency updates, these are the noteworthy changes:

* Migration from Todoist now uses their new v9 api. As a result, syncing with Todoist should work properly now.
* Other issues with the migration from third-party services have been fixed.
* A problem with sending overdue task reminders has been fixed.
* Resetting the user's name to an empty name now works as expected.
* All support for a Wunderlist import has been removed as they shut down their api a while ago.
* When assigning a user with quick-add-magic to a newly created task, Vikunja now correctly ignores users who don't exist or don't have access to the task instead of removing the username text from the task title.
* When switching between tasks directly via the related task list the task description of the new task is now displayed correctly.
* Norwegian is now available as a language for the interface. Thanks to our volunteer translators!

As usual, you can find the full changelogs in the [frontend](https://kolaente.dev/vikunja/frontend/src/branch/main/CHANGELOG.md#0-20-3-2023-01-24) and [api](https://kolaente.dev/vikunja/api/src/branch/main/CHANGELOG.md#0-20-2-2023-01-24) repos.

To get the upgrade, simply replace the frontend files or api binary or pull the latest docker image.
You can also check out the update docs for the [frontend](https://vikunja.io/docs/install-frontend/#updating) or [api](https://vikunja.io/docs/install-backend/#updating) updating process.

